import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './routes'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'

axios.defaults.baseURL = process.env.VUE_APP_API_URL

createApp(App)
  .use(VueAxios, axios)
  .use(store)
  .use(router)
  .mount('#app')
