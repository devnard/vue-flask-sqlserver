import jwt
import json
from flask import jsonify, request, g
from functools import wraps
from app.data.loader import DataLoader

from app.auth.constaints import SECRET
from app.user.models.user import user

dl = DataLoader()

def token_required(f):
   @wraps(f)
   def decorator(*args, **kwargs):
    
        token = None

        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']

        if 'Authorization' in request.headers:
            
            try:
                token = request.headers['Authorization'].split('Bearer ')[1]
            except:
                pass

        if not token:
            return jsonify({'message': 'a valid token is missing'})

        try:
            data = jwt.decode(token, SECRET)
            g.user = user(dl.load('users').get('uid'))
        except Exception as e:
            print(str(e))
            return jsonify({'message': 'token is invalid'})

        return f(*args, **kwargs)
   return decorator