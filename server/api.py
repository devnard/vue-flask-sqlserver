from flask import Flask
from flask_cors import CORS
from flask_restful import reqparse, abort, Api

from app.auth.api import AuthAPI
from app.user.api import userAPI, userListAPI

app = Flask(__name__)

cors = CORS(
    app, 
    resources={r"/*": {"origins": "*"}}, 
    allow_headers=[
        "Content-Type", 
        "Authorization", 
        "Access-Control-Allow-Credentials"
    ],
    supports_credentials=True
)

api = Api(app)

## Actually setup the Api resource routing here
##
api.add_resource(AuthAPI, '/auth/<action>')
api.add_resource(userListAPI, '/user-list')
api.add_resource(userAPI, '/user/<user_id>')

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  return response

if __name__ == '__main__':
    app.run(debug=True)