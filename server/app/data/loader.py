import os
import json
import pyodbc
import pickle
from time import sleep

from app.settings import (
    SQL_DATABASE_DRIVER,
    SQL_DATABASE_HOST,
    SQL_DATABASE_USER,
    SQL_DATABASE_PASSWORD,
    SQL_DATABASE_DEFAULT
)

cached_resource = {}

DB_CONNECTION = None

if not DB_CONNECTION:
    DB_CONNECTION = pyodbc.connect('Driver={' + SQL_DATABASE_DRIVER + '};' + f'SERVER={SQL_DATABASE_HOST};DATABASE={SQL_DATABASE_DEFAULT};UID={SQL_DATABASE_USER};PWD={SQL_DATABASE_PASSWORD};MultipleActiveResultSets=True')

class DataLoader():

    db_connection = DB_CONNECTION

    def __init__(self):
        self.root_dir = os.path.realpath(os.path.dirname(__file__))
        self.max_tries = 100
        self.tries = 0

    def query_to_json(self, query, primary_key=None, params=None, limit=None):
        result = None

        try:

            if limit:
                query = query.replace('SELECT', f'SELECT TOP {limit}')

            cursor = self.query(f""" \
                    DECLARE @result NVARCHAR(max);

                    SET @result = ({query}
                            FOR JSON AUTO, ROOT('data'))

                    SELECT @result;\
                """, params=params)
            
            json_parts = cursor.fetchone()

            result = json.loads(''.join(json_parts)).get('data')

            if primary_key:
                _result = {}
                
                for item in result:
                    _result[item.get(primary_key)] = item

                result = _result        

        except Exception as e:
            print(f"[ERROR][DataLoader][load] {str(e)}")
        
        return result

    def query(self, query, primary_key=None, params=None, limit=None):
        print(query, params)

        if limit:
            query = query.replace('SELECT ', f'SELECT TOP {limit}')

        def _run():
            result = None

            try:
                cursor = self.db_connection.cursor()

                if params:
                    cursor.execute(f"{query}", *params)
                else:
                    cursor.execute(f"{query}")

                result = cursor
            except Exception as e:
                if 'HY000' not in str(e):
                    print(f"[ERROR][DataLoader][query] {str(e)}")
                elif self.tries <= self.max_tries:
                    self.tries += 1
                    sleep(5)
                    return _run()
            
            return result
        
        result = _run()

        return result

    def load(self, resource, primary_key=None, where_clause=None, params=None, limit=None):

        result = None

        try:
            result = self.query_to_json(f"SELECT * FROM dexit.dbo.{resource} {where_clause}", primary_key, params, limit)
            # print("Not result", result)
            if not result:
                raise("db results not found")
        except Exception as e:
            print(f"[ERROR][DataLoader][loads] {str(e)}")
            print(f"[INFO][DataLoader][load] Trying json")
            
            result = self.load_json_resource(resource)

            if result:
                print(f"[INFO][DataLoader][load] JSON Data found {len(result)}")

        return result

    def resource_path(self, resource):
        return os.path.join(self.root_dir, f'resource/{resource}')
        
    def save_class_state(self, resource, cl):
        with open(self.resource_path(f'class/{resource}') + '.dat', 'wb') as f:
            pickle.dump(cl, f)
    
    def load_class_state(self, resource):
        state = None

        try:
            with open(self.resource_path(f'class/{resource}') + '.dat', 'rb') as f:
                state = pickle.load(f)
            print(state)
        except Exception as e:
            print('dl.load_class_state error', str(e))
            pass

        return state

    def save_json_resource(self, resource, data):
        with open(self.resource_path(resource) + '.json', 'w') as f:
            json.dump(data, f)

    def load_json_resource(self, resource):
        result = None

        try:
            if resource + 'ss' in cached_resource:
                print(f"[INFO][DataLoader][load] cached_resource: {resource}")
                result = cached_resource[resource]
            else:
                resource_path = self.resource_path(resource) + '.json'
                with open(resource_path) as f:
                    print(f"[INFO][DataLoader][load] resource: {resource_path}")
                    result = json.load(f)
                    cached_resource[resource] = result
        except Exception as e:
            print(f"[ERROR][DataLoader][load_json_resource] {str(e)}")
            print(f"[ERROR][DataLoader][load_json_resource] unable to load this resource({resource})")

        return result