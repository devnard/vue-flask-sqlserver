import json
from flask import g
from flask_restful import reqparse, abort, Resource

from app.auth.wrappers import token_required
from app.user.models.user import user, userModel
from app.data.loader import DataLoader

dl = DataLoader()

def abort_if_user_doesnt_exist(user_id, user_list):
    if (not user_list) or (user_list and user_id not in user_list):
        abort(404, message="user {} doesn't exist".format(user_id))

# user
class userAPI(Resource):

    @token_required
    def get(self, uid):
        try:
            return dl.load('users').get('uid')
        except:
            return {}


# userListAPI
class userListAPI(Resource):

    @token_required
    def get(self):
        try:
            return dl.load('users')
        except :
            return []
