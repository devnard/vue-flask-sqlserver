import json
import jwt
from flask_restful import reqparse, abort, Resource
from flask_cors import cross_origin

from app.data.loader import DataLoader
from app.auth.constaints import SECRET

from app.user.models.user import user

dl = DataLoader()

# Auth
# shows a single user item and lets you delete a user item
class AuthAPI(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()

        self.parser.add_argument('username', type=str, help='user username')
        self.parser.add_argument('password', type=str, help='user password')
    
    def post(self, action):
        args = self.parser.parse_args()

        username = args.get('username')
        password = args.get('password')

        def login():

            try:

                # TEMP
                uid = dl.load('user-credentials').get(f"{username}.{password}")
                print(f"{username}.{password}", 'UID', uid)
                user = dl.load('users').get(uid)
                # /TEMP

                if user:

                    return {
                        "token": str(jwt.encode({'uid': uid}, SECRET, algorithm='HS256'), "utf-8"),
                        "user": user
                    }
        
            except Exception as e:
                print(f"[ERROR][Auth][post] {str(e)}")
                abort(404, message="Incorrect username or password")

        actions = {
            'login': login
        }
        
        return actions.get(action)()