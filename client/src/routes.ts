import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import user_routes from './user/routes'

import Home from './views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  ...[
    {
      path: '/',
      name: 'Home',
      component: Home
    }
  ],
  ...user_routes
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
