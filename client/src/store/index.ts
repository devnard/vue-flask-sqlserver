import { createStore, StoreOptions } from 'vuex'
import axios from 'axios'
import { VIEWS } from '@/costants'

const headers = () => {
  return {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  }
}

const api = {
  get: (url, config?: {}) => {
    return axios.get(url, {
      ...config,
      ...headers()
    })
  },
  post: (url, data, config?: {}) => {
    return axios.post(url, data, {
      ...config,
      ...headers()
    })
  }
}

// eslint-disable-next-line
const mainStoreOptions: StoreOptions<any> = {
  state: {
    api: api,
    constants: {
      VIEWS
    },
    view: localStorage.getItem('view') || 'login',
    token: localStorage.getItem('token')
  },
  mutations: {
    setView (state, payload) {
      state.view = payload
      localStorage.setItem('view', payload)
    },
    setToken (state, payload) {
      state.token = payload
      localStorage.setItem('token', payload)
    }
  },
  actions: {
    updateView (state, payload) {
      state.commit('setView', payload)
    }
  },
  modules: {
  },
  getters: {
    getAPI (state) {
      return state.api
    },
    getCurrentView (state) {
      return state.view
    }
  }
}

// eslint-disable-next-line
const userStoreOptions: StoreOptions<any> = {
  state: {
    user: JSON.parse(localStorage.getItem('user') || '{}'),
    loadingUser: false
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
      localStorage.setItem('user', JSON.stringify(payload))
    }
  },
  actions: {
    updateUser (store) {
      if (!store.state.loadingUser) {
        store.state.loadingUser = true

        api.get(`user/${store.state.user.uid}`).then(result => {
          console.log('user results', result)

          if ('uid' in result.data) {
            store.state.loadingUser = false
            store.commit('setUser', result.data)
          }
        }).catch(error => {
          throw new Error(`API ${error}`)
        })
      }
    }
  },
  getters: {
    getUser: state => state.user
  }
}

// eslint-disable-next-line
const loginStoreOptions: StoreOptions<any> = {
  state: {
    loginUsername: '',
    loginPassword: '',
    loginSubmiting: false
  },
  mutations: {
    setLoginUsername (state, payload) {
      state.loginUsername = payload
    },
    setLoginPassword (state, payload) {
      state.loginPassword = payload
    }
  },
  actions: {
    updateLoginUsername (store, payload) {
      store.commit('setLoginUsername', payload)
    },
    updateLoginPassword (store, payload) {
      store.commit('setLoginPassword', payload)
    },
    submitLogin (store) {
      if (!store.state.loginSubmiting) {
        store.state.loginSubmiting = true

        const payload = {
          username: store.state.loginUsername,
          password: store.state.loginPassword
        }

        console.log('Login:', store.state.loginSubmiting, payload)

        api.post('auth/login', payload).then(result => {
          console.log('results', result)

          if ('token' in result.data) {
            store.commit('setToken', result.data.token)
          }

          if ('user' in result.data) {
            store.commit('setUser', result.data.user)
            store.dispatch('updateUser')
          }
        }).catch(error => {
          throw new Error(`API ${error}`)
        }).finally(() => {
          store.state.loginSubmiting = false
        })
      }
    }
  },
  getters: {
    getLoginUsername: state => state.loginUsername,
    getLoginPassword: state => state.loginPassword,
    getLoginSubmiting: state => state.loginSubmiting
  }
}

// eslint-disable-next-line
const combineStoreOptions = (storeOps: any): StoreOptions<any> => {
  // eslint-disable-next-line
  const combinedOptions: any = {};
  // eslint-disable-next-line
  const combinedKeys: any = {};

  (Object.keys(storeOps)).forEach(key => {
    (Object.keys(storeOps[key])).forEach(opKey => {
      combinedKeys[opKey] = true
    })
  });

  (Object.keys(storeOps)).forEach(key => {
    const storeOp = storeOps[key];

    (Object.keys(combinedKeys)).forEach(combinedKey => {
      if (combinedKey in storeOp) {
        combinedOptions[combinedKey] = { ...combinedOptions[combinedKey], ...storeOp[combinedKey] }
      }
    })
  })

  return combinedOptions
}

export default createStore(combineStoreOptions({
  mainStoreOptions,
  userStoreOptions,
  loginStoreOptions
}))
