# This code parses date/times, so please
#
#     pip install python-dateutil
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = user_model_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Optional, Any, TypeVar, Type, cast
from datetime import datetime
import dateutil.parser


T = TypeVar("T")


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class userModel:
    id: Optional[int] = None
    name: Optional[str] = None
    group_id: Optional[int] = None
    role_id: Optional[int] = None
    date_created: Optional[datetime] = None
    updated_date: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'userModel':
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        group_id = from_union([from_int, from_none], obj.get("group_id"))
        role_id = from_union([from_int, from_none], obj.get("role_id"))
        date_created = from_union([from_datetime, from_none], obj.get("date_created"))
        updated_date = from_union([from_datetime, from_none], obj.get("updated_date"))
        return userModel(id, name, group_id, role_id, date_created, updated_date)

    def to_dict(self) -> dict:
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["group_id"] = from_union([from_int, from_none], self.group_id)
        result["role_id"] = from_union([from_int, from_none], self.role_id)
        result["date_created"] = from_union([lambda x: x.isoformat(), from_none], self.date_created)
        result["updated_date"] = from_union([lambda x: x.isoformat(), from_none], self.updated_date)
        return result


def user_model_from_dict(s: Any) -> userModel:
    return userModel.from_dict(s)

def user_model_to_dict(x: userModel) -> Any:
    return to_class(userModel, x)
