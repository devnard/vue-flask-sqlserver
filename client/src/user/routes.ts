import { RouteRecordRaw } from 'vue-router'
import Dashboard from './views/Dashboard.vue'

const user_routes: Array<RouteRecordRaw> = [
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  }
]

export default user_routes
