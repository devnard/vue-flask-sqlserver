from app.data.loader import DataLoader
from app.user.models.model import userModel

import random

dl = DataLoader()

class user():
    def __init__(self, user_dict):
        self.Model = userModel.from_dict(user_dict)

    def get(self, attr):

        try:
            return getattr(self.Model, attr)
        except Exception as e:
            print('ERRROR', e)
            return self.getattr(attr)
